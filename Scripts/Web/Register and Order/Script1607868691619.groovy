import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Date today = new Date()

String timeVar = today.format('hhmmss')

String emailVar = ('KSAT002ONL002_' + timeVar) + '@thanks.hacktiv8'

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('Web/Page_DemoShop/a_Register as a new user'))

//email already exist
WebUI.setText(findTestObject('Web/Page_DemoShop/input_Email_Input.Email'), 'ksat002-002@mailsac.com')

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Password_Input.Password'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Confirm password_Input.ConfirmPassword'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.click(findTestObject('Web/Page_DemoShop/button_Register'))

//not email format
WebUI.setText(findTestObject('Web/Page_DemoShop/input_Email_Input.Email'), 'ksat002-002')

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Password_Input.Password'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Confirm password_Input.ConfirmPassword'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.click(findTestObject('Web/Page_DemoShop/button_Register'))

//password != confirm password
WebUI.setText(findTestObject('Web/Page_DemoShop/input_Email_Input.Email'), emailVar)

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Password_Input.Password'), 'YCUh5pXRbwTAeeiT9NgDrw==')

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Confirm password_Input.ConfirmPassword'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.click(findTestObject('Web/Page_DemoShop/button_Register'))

//email and password true
WebUI.setText(findTestObject('Web/Page_DemoShop/input_Email_Input.Email'), emailVar)

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Password_Input.Password'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.setEncryptedText(findTestObject('Web/Page_DemoShop/input_Confirm password_Input.ConfirmPassword'), 'OJwx8SYthuJvTtSTHlbXgw==')

WebUI.click(findTestObject('Web/Page_DemoShop/button_Register'))

WebUI.click(findTestObject('Web/Page_DemoShop/a_Profile'))

WebUI.verifyTextPresent(emailVar, false)

WebUI.setText(findTestObject('Web/Page_DemoShop/input_Phone number_Input.PhoneNumber'), '9876789000')

WebUI.click(findTestObject('Web/Page_DemoShop/button_Save'))

WebUI.verifyTextPresent('Your profile has been updated', false)

WebUI.click(findTestObject('Web/Page_DemoShop/span_'))

WebUI.click(findTestObject('Web/Page_DemoShop/a_Links'))

WebUI.click(findTestObject('Web/Page_DemoShop/a_DemoShop'))

WebUI.callTestCase(findTestCase('Web/Verify Product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Web/Page_DemoShop/button_Buy now - mouse'))

WebUI.verifyTextPresent('Computer mouse', false)

WebUI.verifyTextPresent('€8.00', false)

WebUI.click(findTestObject('Web/Page_DemoShop/button_Pay by card'))

WebUI.verifyTextPresent('Computer mouse', false)

WebUI.verifyTextPresent('€8.00', false)

WebUI.setText(findTestObject('Web/Page_DemoShop/input_Card number_Number'), '2015541713774901')

WebUI.setText(findTestObject('Web/Page_DemoShop/input_Cardholder name_Name'), 'Pandya P')

WebUI.setText(findTestObject('Web/Page_DemoShop/input_Expiration date_ExpiryDate'), '12/24')

WebUI.setText(findTestObject('Web/Page_DemoShop/input_Security code_SecurityCode'), '416')

WebUI.click(findTestObject('Web/Page_DemoShop/button_Pay'))

WebUI.verifyTextPresent('Completed', false)

WebUI.click(findTestObject('Web/Page_DemoShop/button_Logout'))

WebUI.verifyTextPresent('Login', false)

