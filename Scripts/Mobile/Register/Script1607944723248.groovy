import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\DIV KJT\\QA\\QA DOC\\DOKUMEN QA\\Training\\HACKTIV8\\Final Project\\FINALPROJECTS-201016-121328\\FINAL PROJECTS\\MOBILE\\app-debug.apk', 
    true)

Mobile.tap(findTestObject('Object Repository/Mobile/android.widget.Button - CREATE A PROFILE'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/android.widget.EditText - First Name'), 'Pandya', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/android.widget.EditText - Last Name'), 'Panditatwa', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/android.widget.EditText - Country'), 'Indonesia', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/android.widget.EditText - Username'), 'pandyapan', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/android.widget.EditText - Password'), 'Admin123', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/android.widget.EditText - Confirm Password'), 'Admin123', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/android.widget.Button - Create Profile'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/android.widget.Button - LOGIN'), 0)

WebUI.callTestCase(findTestCase('Mobile/Add Account'), [:], FailureHandling.STOP_ON_FAILURE)

