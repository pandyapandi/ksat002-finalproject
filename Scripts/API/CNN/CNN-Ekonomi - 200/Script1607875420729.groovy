import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject

//index Ekonomi - true topic
ResponseObject response = WS.sendRequest(findTestObject('API/CNN/CNN-Ekonomi', [('topic') : 'ekonomi']))

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'data[2].judul', GlobalVariable.judul_api)

WS.verifyElementPropertyValue(response, 'data[2].link', GlobalVariable.url_detail_api)

WS.verifyElementPropertyValue(response, 'data[2].poster', 'https://akcdn.detik.net.id/visual/2015/12/03/efa0b80c-2553-467e-a8e2-2b283bc647e2_169.jpg?w=270&q=90')

WS.verifyElementPropertyValue(response, 'data[2].tipe', 'Ekonomi')

WebUI.callTestCase(findTestCase('API/CNN/CNN-Ekonomi - 404'), [:], FailureHandling.STOP_ON_FAILURE)

