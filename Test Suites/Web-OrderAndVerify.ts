<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Web-OrderAndVerify</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ddf8f7b9-0eea-4fbc-90d8-21b2a82a5860</testSuiteGuid>
   <testCaseLink>
      <guid>1c72a7af-47a8-42b4-a775-fd2f9fa0b5a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/Register and Order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2df7c06-1c84-450e-b4ad-121dd7e45226</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/Verify Order</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
