<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>API-CNN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e18dc8bd-f376-4c04-9270-353c20813b36</testSuiteGuid>
   <testCaseLink>
      <guid>d388c9c9-acc8-4229-8989-a4bbf74fcb84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/CNN/CNN-Ekonomi - 200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea43b2f3-416d-4c0f-8ded-c69b21cf3da6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/CNN/CNN-Search - 200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee08abcf-0858-4c13-a63f-fae014aaf3d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/CNN/CNN-Detail - 200</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
