<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Bisatopup-CekTagihan</name>
   <tag></tag>
   <elementGuidId>89e29a49-1e8d-4980-8785-667ccda5a581</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\&quot;product\&quot;: \&quot;${product}\&quot;,\n\&quot;phone_number\&quot;: \&quot;${phone}\&quot;,\n\&quot;nomor_rekening\&quot;: \&quot;${no_rek}\&quot;\n}\n&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>text/plain</value>
   </httpHeaderProperties>
   <katalonVersion>7.7.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://private-anon-2dfc834764-bisatopup.apiary-mock.com/tagihan/cek</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.product</defaultValue>
      <description></description>
      <id>9c2978c7-cdba-4583-beb4-a7e558cb4cae</id>
      <masked>false</masked>
      <name>product</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.phone</defaultValue>
      <description></description>
      <id>9795446c-2a6a-4c53-8c0a-0c413df9e1e7</id>
      <masked>false</masked>
      <name>phone</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.no_rek</defaultValue>
      <description></description>
      <id>5d339c9c-e938-4757-ae1b-e56b44b635c1</id>
      <masked>false</masked>
      <name>no_rek</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

WS.verifyElementPropertyValue(response, 'message', 'Cek tagihan berhasil')
WS.verifyElementPropertyValue(response, 'data.jumlah_tagihan', '137995')
WS.verifyElementPropertyValue(response, 'data.jumlah_bayar', '138995')
WS.verifyElementPropertyValue(response, 'data.admin', '2500')
WS.verifyElementPropertyValue(response, 'data.terbayar', '0')
WS.verifyElementPropertyValue(response, 'data.nama', 'SYAIBAN')
WS.verifyElementPropertyValue(response, 'data.periode', '201608')</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
