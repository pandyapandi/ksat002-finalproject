<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>CNN-Detail</name>
   <tag></tag>
   <elementGuidId>3e081a51-d594-4316-8728-76b797985401</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <katalonVersion>7.7.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://www.news.developeridn.com/detail/?url=${url}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url_detail_api</defaultValue>
      <description></description>
      <id>bf45ebad-a45e-464b-a695-362ae0d1fb11</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)
WS.verifyElementPropertyValue(response, 'data[0].body', &quot;\n                        Jakarta, CNN Indonesia -- Majalah Forbes mencatat ada empat konglomerat Indonesia yang terdepak dari daftar orang kaya versi mereka. Mulai dari pendiri Bosowa Group hingga pemilik Lion Air.\nMenurut Forbes, para konglomerat ini keluar dari daftar karena nilai kekayaannya menurun pada tahun ini. Hal ini tak lepas dari tekanan ekonomi akibat pandemi virus corona atau Covid-19.\nSelain itu, pertumbuhan ekonomi Indonesia yang terkontraksi ke kisaran 5,32 persen dan minus 3,49 persen pada kuartal II dan III turut menambah lesu bisnis mereka.\n\n\n\n\n\n\n\n\n\n\nLihat juga:\nDua Srikandi yang Masuk Dalam Daftar Orang Terkaya di RI\n\n\n\n\nPertama, Aksa Mahmud. Pendiri Bosowa Corp yang juga merupakan politikus Partai Golkar ini terdepak dari daftar orang terkaya di Indonesia versi Forbes. Tahun lalu, ia menempati posisi ke-44 dengan kekayaan bersih US$740 juta.\nKedua, Donald Sihombing. Ia merupakan pemegang saham terbesar PT Totalindo Eka Persada Tbk (TOPS).\nPada 2018 harga saham Totalindo sempat mencapai Rp 990 per saham. Pada harga saham tersebut, nilai kapitalisasi saham Totalindo sempat mencapai Rp33 triliun.\nIa terdepak dari daftar setelah pada tahun lalu menempati posisi ke-34 dengan kekayaan US$970 juta.\nKetiga, Kardja Rahardjo. Ia menjalankan perusahaan pelayaran Pelayaran Tamarin Samudra, yang didirikannya pada tahun 1998.\n\n\n\n\n\nLihat juga:\nLima Konglomerat Muda yang Masuk Daftar Orang Terkaya RI\n\n\n\n\nPerusahaan menyediakan layanan sewa kapal itu memiliki berbagai macam klien termasuk Perusahaan Minyak Lepas Pantai Nasional China. Sahamnya tercatat di Bursa Efek Indonesia pada Mei 2017 dan saat itu memiliki hampir 80 persen saham.\nTahun lalu ia berada di posisi ke-31 dengan kekayaan bersih US$1,02 miliar\nKeempat, kakak beradik Kusnan dan Rusdi Kirana. Mereka keluar dari daftar konglomerat versi Forbes di tengah pandemi karena perusahaan maskapai Lion Air yang mereka kelola menderita karena anjloknya perjalanan udara.\nTahun lalu, mereka menempati posisi ke-38 dengan kekayaan US$835 juta. (uli/asr)\n\n\n\n\n \n\n[Gambas:Video CNN]\n\n\n\n\n&quot;)
WS.verifyElementPropertyValue(response, 'data[0].judul', &quot;\n                    Konglomerat yang Terdepak dari Daftar Orang Terkaya RI                &quot;)
WS.verifyElementPropertyValue(response, 'data[0].poster', &quot;https://akcdn.detik.net.id/visual/2015/12/03/efa0b80c-2553-467e-a8e2-2b283bc647e2_169.jpg?w=650&quot;)
WS.verifyElementPropertyValue(response, 'data[0].message', &quot;network error&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
