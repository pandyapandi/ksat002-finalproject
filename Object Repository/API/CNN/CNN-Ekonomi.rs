<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>CNN-Ekonomi</name>
   <tag></tag>
   <elementGuidId>f6899d6d-08de-442d-85b6-7d335c1b9d65</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <katalonVersion>7.7.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://www.news.developeridn.com/${topic}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'ekonomi'</defaultValue>
      <description></description>
      <id>5e6e10d7-3546-43ff-945b-fd77eed382bd</id>
      <masked>false</masked>
      <name>topic</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)
WS.verifyElementPropertyValue(response, 'data[2].judul', &quot;Konglomerat yang Terdepak dari Daftar Orang Terkaya RI&quot;)
WS.verifyElementPropertyValue(response, 'data[2].link', &quot;https://www.cnnindonesia.com/ekonomi/20201213104617-92-581462/konglomerat-yang-terdepak-dari-daftar-orang-terkaya-ri&quot;)
WS.verifyElementPropertyValue(response, 'data[2].poster', &quot;https://akcdn.detik.net.id/visual/2015/12/03/efa0b80c-2553-467e-a8e2-2b283bc647e2_169.jpg?w=270&amp;q=90&quot;)
WS.verifyElementPropertyValue(response, 'data[2].tipe', &quot;Ekonomi&quot;)
WS.verifyElementPropertyValue(response, 'data[2].waktu', &quot; \u2022 11 jam yang lalu&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
